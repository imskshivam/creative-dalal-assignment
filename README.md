# Getting Started
## Step 1: Run the Code
To start using the application, first ensure you have Flutter installed. You can follow the official Flutter installation guide for your operating system.

Clone the repository:
```bash
git clone https://gitlab.com/imskshivam/creative-dalal-assignment 
```
Navigate to the project directory:

```bash
cd creative-dalal-assignment
```
Get the dependencies:
```bash
flutter pub get
```
Run the application:

flutter run
## Step 2: Install the Application
After running the code, you can install the application on your device. Follow the platform-specific instructions below.

#For Android:
Connect your Android device or start an emulator.
Run the application:
```bash
flutter run
```
For iOS:
Connect your iOS device or start a simulator.
Run the application:
```bash
flutter run
```
 # Application Features
## Deleting a Comment
Action: Long press on a comment.
Description: This will delete the comment.
## Adding a Comment
Action: Write in the text field and submit.
Description: This will add a new comment to the list.
## Editing a Comment
Action: Double tap on the comment.
Description: This will allow you to edit the comment.
## Replying to a Comment
Action: Click on the reply text.
Description: This will open a text field to write a reply to the selected comment.
## Reply Management
Action: Multiple replies to any comment will collapse automatically.
Description: View all replies by clicking on the collapsed view.

#DEMO VIDEO LINK

https://drive.google.com/file/d/1-bIEy0Wq3URnmVEsrc0EnapUn_XtVXO-/view?usp=sharing