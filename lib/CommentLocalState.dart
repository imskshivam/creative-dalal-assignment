// lib/comment_local_state.dart
import 'package:flutter/foundation.dart';

class CommentLocalState with ChangeNotifier {
  bool _isEditing = false;
  bool _showAllReplies = false;

  bool get isEditing => _isEditing;
  bool get showAllReplies => _showAllReplies;

  void toggleEditing() {
    _isEditing = !_isEditing;
    notifyListeners();
  }

  void setEditing(bool isEditing) {
    _isEditing = isEditing;
    notifyListeners();
  }

  void toggleShowAllReplies() {
    _showAllReplies = !_showAllReplies;
    notifyListeners();
  }

  void setShowAllReplies(bool showAllReplies) {
    _showAllReplies = showAllReplies;
    notifyListeners();
  }
}
