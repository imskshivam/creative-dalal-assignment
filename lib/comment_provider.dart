// lib/providers/comment_provider.dart
import 'package:flutter/foundation.dart';

import 'package:flutter_application_1/commentModel.dart';

class CommentProvider with ChangeNotifier {
  List<Comment> _comments = [];

  List<Comment> get comments => _comments;

  void addComment(String content, {String parentId = ''}) {
    _comments.add(Comment.newComment(content, parentId: parentId));
    notifyListeners();
  }

  void editComment(String id, String newContent) {
    final comment = _comments.firstWhere((comment) => comment.id == id);
    comment.content = newContent;
    notifyListeners();
  }

  void deleteComment(String id) {
    _comments.removeWhere((comment) => comment.id == id);
    notifyListeners();
  }

  List<Comment> getReplies(String parentId, {int limit = 4}) {
    List<Comment> replies =
        _comments.where((comment) => comment.parentId == parentId).toList();
    return replies.take(limit).toList();
  }

  int countReplies(String parentId) {
    return _comments.where((comment) => comment.parentId == parentId).length;
  }

  List<Comment> getAllReplies(String parentId) {
    return _comments.where((comment) => comment.parentId == parentId).toList();
  }
}
