// lib/widgets/comment_widget.dart
import 'package:flutter/material.dart';
import 'package:flutter_application_1/CommentLocalState.dart';
import 'package:flutter_application_1/commentModel.dart';
import 'package:flutter_application_1/comment_provider.dart';
import 'package:provider/provider.dart';

class CommentWidget extends StatelessWidget {
  final Comment comment;
  final bool isReply;
  final int initialDisplayLimit = 4;

  CommentWidget({required this.comment, this.isReply = false, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => CommentLocalState(),
      child: Padding(
        padding: EdgeInsets.only(left: isReply ? 20.0 : 0),
        child: Consumer<CommentLocalState>(
          builder: (context, localState, child) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: Icon(Icons.usb_rounded),
                      ),
                    ),
                    if (localState.isEditing)
                      Expanded(
                        child: TextField(
                          controller:
                              TextEditingController(text: comment.content),
                          autofocus: true,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50),
                              borderSide: BorderSide(color: Colors.black26),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50),
                              borderSide: BorderSide(color: Colors.black26),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50),
                              borderSide: BorderSide(color: Colors.blue),
                            ),
                          ),
                          onSubmitted: (newContent) {
                            _editComment(context, newContent);
                          },
                        ),
                      )
                    else
                      Expanded(
                        child: GestureDetector(
                          onDoubleTap: () {
                            localState.toggleEditing();
                          },
                          onLongPress: () {
                            _showDeleteDialog(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              border: Border.all(color: Colors.black26),
                            ),
                            child: Text(
                              comment.content,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 5,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        _showReplyDialog(context);
                      },
                      child: Padding(
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        child: Text(
                          "Reply",
                          style: TextStyle(color: Colors.blue),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 10, top: 5, bottom: 5, right: 10),
                      child: Text(
                        comment.timestamp.toString(),
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  ],
                ),
                Consumer<CommentProvider>(
                  builder: (context, commentProvider, child) {
                    final replies = localState.showAllReplies
                        ? commentProvider.getAllReplies(comment.id)
                        : commentProvider.getReplies(comment.id);
                    final totalReplies =
                        commentProvider.countReplies(comment.id);
                    final displayReplies = localState.showAllReplies
                        ? replies
                        : replies.take(initialDisplayLimit).toList();
                    final remainingReplies = totalReplies - initialDisplayLimit;
                    return Column(
                      children: [
                        ...displayReplies
                            .map((reply) => CommentWidget(
                                  key: ValueKey(reply.id),
                                  comment: reply,
                                  isReply: true,
                                ))
                            .toList(),
                        if (remainingReplies > 0 && !localState.showAllReplies)
                          TextButton(
                            child:
                                Text('View more replies ($remainingReplies)'),
                            onPressed: () {
                              localState.toggleShowAllReplies();
                            },
                          ),
                      ],
                    );
                  },
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  void _editComment(BuildContext context, String newContent) {
    final commentProvider =
        Provider.of<CommentProvider>(context, listen: false);
    commentProvider.editComment(comment.id, newContent);
    Provider.of<CommentLocalState>(context, listen: false).setEditing(false);
  }

  void _showDeleteDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Delete Comment'),
          content: Text('Are you sure you want to delete this comment?'),
          actions: [
            TextButton(
              child: Text('Cancel'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            TextButton(
              child: Text('Delete'),
              onPressed: () {
                Provider.of<CommentProvider>(context, listen: false)
                    .deleteComment(comment.id);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showReplyDialog(BuildContext context) {
    final _replyController = TextEditingController();
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Reply to Comment'),
          content: TextField(
            controller: _replyController,
            decoration: InputDecoration(hintText: 'Enter your reply'),
          ),
          actions: [
            TextButton(
              child: Text('Cancel'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            TextButton(
              child: Text('Reply'),
              onPressed: () {
                Provider.of<CommentProvider>(context, listen: false)
                    .addComment(_replyController.text, parentId: comment.id);
                _replyController.clear();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
