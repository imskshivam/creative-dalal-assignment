import 'package:uuid/uuid.dart';

class Comment {
  String id;
  String content;
  String parentId;
  DateTime timestamp;

  Comment({
    required this.id,
    required this.content,
    this.parentId = '',
    required this.timestamp,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'content': content,
      'parentId': parentId,
      'timestamp': timestamp.toIso8601String(),
    };
  }

  factory Comment.fromMap(Map<String, dynamic> map) {
    return Comment(
      id: map['id'],
      content: map['content'],
      parentId: map['parentId'],
      timestamp: DateTime.parse(map['timestamp']),
    );
  }

  factory Comment.newComment(String content, {String parentId = ''}) {
    return Comment(
      id: Uuid().v4(),
      content: content,
      parentId: parentId,
      timestamp: DateTime.now(),
    );
  }

  Comment copyWith({
    String? id,
    String? content,
    String? parentId,
    DateTime? timestamp,
  }) {
    return Comment(
      id: id ?? this.id,
      content: content ?? this.content,
      parentId: parentId ?? this.parentId,
      timestamp: timestamp ?? this.timestamp,
    );
  }
}
